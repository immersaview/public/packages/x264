cmake_minimum_required(VERSION 3.0)

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "aarch64")
    set(ARCH_DIR "arm64")
elseif ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

# Set PLATFORM_DIR
set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")
if (UNIX)
    find_program(LSB_RELEASE_EXEC lsb_release)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} --codename OUTPUT_VARIABLE LSB_RELEASE_CODENAME OUTPUT_STRIP_TRAILING_WHITESPACE)

    if (LSB_RELEASE_CODENAME MATCHES "xenial")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu16_04/gcc5_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "bionic")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu18_04/gcc7_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "focal")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu20_04/gcc9_3_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_CODENAME MATCHES "Core")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/centos7/gcc7_2_1/${ARCH_DIR}")
    endif ()

elseif (WIN32)
    set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/windows/vc142/${ARCH_DIR}")
endif ()

function(add_imported_lib PLATFORM_DIR LIB MAJOR_VERSION)
    message(STATUS "Adding shared library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" SHARED IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${PLATFORM_DIR}/include")

    if (UNIX)
        set(LIB_SO  "${CMAKE_SHARED_LIBRARY_PREFIX}${LIB}${CMAKE_SHARED_LIBRARY_SUFFIX}.${MAJOR_VERSION}")

        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/${LIB_SO}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG   "${PLATFORM_DIR}/debug/${LIB_SO}")
    elseif (WIN32)
        set(LIB_DYNAMIC "lib${LIB}-${MAJOR_VERSION}${CMAKE_SHARED_LIBRARY_SUFFIX}")
        set(LIB_STATIC  "lib${LIB}${CMAKE_SHARED_LIBRARY_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")

        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/${LIB_DYNAMIC}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_IMPLIB_RELEASE   "${PLATFORM_DIR}/release/${LIB_STATIC}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG   "${PLATFORM_DIR}/debug/${LIB_DYNAMIC}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_IMPLIB_DEBUG     "${PLATFORM_DIR}/debug/${LIB_STATIC}")
    endif ()
    
endfunction(add_imported_lib)

# Add Libraries
add_imported_lib("${PLATFORM_DIR}" "x264" "148")
