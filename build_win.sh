# Usage:
# build_win <HOST> <CONFIG>


# Exit as failure if any command fails
set -e

HOST=$1
CONFIG=$2
ARCH=$3


pushd "source"
    export CC=cl
    PARAMS="--enable-shared --prefix='`pwd`/installed/${CONFIG}' --host=$HOST --disable-gpl --chroma-format=all --bit-depth=8"
    
    if [[ "${CONFIG}" == "Debug" ]]; then
        PARAMS="$PARAMS --enable-debug --extra-cflags=-MDd"
    else
        PARAMS="$PARAMS --extra-cflags=-MD --extra-cflags=-g"
    fi

    ./configure $PARAMS
    make -j `nproc`
    make install
popd
