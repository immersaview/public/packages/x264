**Packaging for x264**

## Example CMake Usage:
```cmake
target_link_libraries(target x264)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:x264,INTERFACE_INCLUDE_DIRECTORIES>")
```

The following options were set during x264 compilation.

```bash
chroma_format=all
interlaced=yes
gpl=no 
bit_depth=8
```
