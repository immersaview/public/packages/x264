@ECHO OFF

REM Usage: build_win <generator> <platform> <config>

SETLOCAL ENABLEDELAYEDEXPANSION

SET PLATFORM=%1
SET CONFIG=%2
REM the '.' is to allow an extra '\' to be added for code clarity eg. "!BASE_DIR!\!GENERATOR!"
SET BASE_DIR=%~dp0.


REM remove " and '
SET PLATFORM=%PLATFORM:"=%
SET PLATFORM=%PLATFORM:'=%

CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" %PLATFORM%
CALL C:\msys64\msys2_shell.cmd -here -full-path -no-start -defterm -c -l "./build_win.sh x86_64-pc-mingw64 %CONFIG% %PLATFORM%" || GOTO :ERROR

XCOPY source\installed artifacts\windows\VisualStudio%PLATFORM%\ /S /Y
IF %CONFIG% EQU Debug (
    XCOPY source\x264.pdb artifacts\windows\VisualStudio%PLATFORM%\Debug\lib\
)

:ERROR
ENDLOCAL
EXIT /B %ERRORLEVEL%
