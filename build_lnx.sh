#!/usr/bin/env bash

# Usage:
# build_lnx <HOST> <CONFIG>


# Exit as failure if any command fails
set -e

HOST=$1
ARCH=$2
CONFIG=$3

pushd "source"
    PARAMS="--enable-shared --prefix='`pwd`/installed/${CONFIG}' --disable-gpl --chroma-format=all --bit-depth=8 --enable-pic"
    
    if [[ "${CONFIG}" == "Debug" ]]; then
        PARAMS="$PARAMS --enable-debug"
    fi
    
    ./configure $PARAMS
    make -j `nproc`
    make install
popd

mkdir -p artifacts/$HOST/$ARCH
cp ./source/installed/* ./artifacts/$HOST/$ARCH -r

